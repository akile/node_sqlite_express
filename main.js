const sqlite3 = require('sqlite3').verbose();
var express = require('express')
var app = express()

app.listen(8080)

app.get('/', function(req, res) {

    let db = new sqlite3.Database('database.db',(err) => {
        if(err) {
            console.log(err.message);
        }
        console.log('Connected to the database.');
    });

    db.serialize(() => {
        db.each(`SELECT * from users`, (err, row) => {
            if(err) {
                console.log(err.message);
            }
            console.log(row.username);
        });
    })
    

    db.close((err) => {
        if (err) {
            console.log(err.message);
        }
        console.log('Database connection closed.')
    })     
})

app.post('/', function (req, res) {
    res.send('POST request to the homepage' )
})